import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { Globals } from './utils/globals';
import { AppRoutingModule } from './app-routing.module';
import { HamburgerbuttonComponent } from './components/hamburgerbutton/hamburgerbutton.component';
import { CharcardComponent } from './components/charcard/charcard.component';
import { CharacterListComponent } from './views/character-list/character-list.component';
import { CharacterDetailsComponent } from './views/character-details/character-details.component';
import { SimplifyHairPipe } from './pipes/simplifyHair/simplify-hair.pipe';
import { HomeComponent } from './views/home/home.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { RoundNumberPipe } from './pipes/roundNumber/round-number.pipe';

@NgModule({
    declarations: [
        AppComponent,
        HamburgerbuttonComponent,
        CharcardComponent,
        CharacterListComponent,
        CharacterDetailsComponent,
        SimplifyHairPipe,
        HomeComponent,
        NotFoundComponent,
        RoundNumberPipe,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [Globals],
    bootstrap: [AppComponent]
})
export class AppModule { }
