import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { catchError, delay } from "rxjs/internal/operators";
import { of } from 'rxjs/internal/observable/of';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ApiclientService {

    constructor(
        private httpClient: HttpClient,
        private router: Router
    ) { }

    read(link: string, directories?: string[], parameters?: Map<string, string>): Observable<any> {
        let finalURI: string = "";
        let lastChar = "";
        let response: Observable<any>;

        finalURI += link;

        lastChar = finalURI.substr(finalURI.length - 1);

        // Build URI: point to a directory
        if (directories != undefined) {
            if (directories.length != 0) {

                // Set ready the URI
                if (lastChar != "/") {
                    finalURI += "/"
                }

                for (let i = 0; i < directories.length; i++) {
                    finalURI += directories[i] + "/";
                }
                finalURI += "?";
            }
        }
        if (parameters != undefined) {
            // Build URI: add parameters
            if (parameters.size != 0) {
                for (let [key, value] of parameters.entries()) {
                    finalURI += key + "=" + value + "&";
                }

                finalURI = finalURI.substr(0, finalURI.length - 1);
            }
        }

        response = this.httpClient.get(finalURI, httpOptions)
        // CATCH API 404 ERROR
        .pipe(catchError((error) => {
            console.log(error);
            if(error.status == "404") {
                this.router.navigateByUrl('404');
            }
            return of(error);
        }) as any);

        return response;
    }

}