import { Injectable } from "@angular/core";

@Injectable()
export class Globals {
    shortcuts = new Map([
        ["base", "https://swapi.co/api"],
        ["people", "https://swapi.co/api/people"]
    ]);
}