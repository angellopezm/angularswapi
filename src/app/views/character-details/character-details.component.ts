import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiclientService } from 'src/app/services/apiclient/apiclient.service';
import { Globals } from 'src/app/utils/globals';
import { ActivatedRoute } from '@angular/router';
import { HamburgerbuttonComponent } from 'src/app/components/hamburgerbutton/hamburgerbutton.component';

@Component({
    selector: 'cdetails-view',
    templateUrl: './character-details.component.html',
    styleUrls: ['./character-details.component.css']
})
export class CharacterDetailsComponent implements OnInit {
    
    @ViewChild(HamburgerbuttonComponent) hamburgerButton: HamburgerbuttonComponent;

    request: Observable<any>;
    response: any;

    planet: string;

    constructor(
        private route: ActivatedRoute,
        private apiClient: ApiclientService,
        private globals: Globals
    ) { }

    ngOnInit() {
        this.loadCharInfo();
    }
    
    loadCharInfo() {
        // REQUEST: Define the request
        this.request = this.apiClient.read(
            this.globals.shortcuts.get("people") + "/" +
            this.route.snapshot.params['id']
        );

        // RESPONSE
        this.response = this.request.subscribe((result) => {
            this.response = result;
        });
    }
}
