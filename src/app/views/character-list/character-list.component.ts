import { Component, OnInit, ViewChild } from '@angular/core';
import { HamburgerbuttonComponent } from 'src/app/components/hamburgerbutton/hamburgerbutton.component';
import { Observable } from 'rxjs';
import { Globals } from 'src/app/utils/globals';
import { ApiclientService } from 'src/app/services/apiclient/apiclient.service';

@Component({
    selector: 'clist-view',
    templateUrl: './character-list.component.html',
    styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {

    request: Observable<any>;
    response: any;

    currentPage: string = this.globals.shortcuts.get("people") + "/?page=1"; // First page

    // Starts as current page, just because when we visit for the first time the next page is the first one. This changes later, take a look to ifNext()
    nextPage: string = this.currentPage; 
    previousPage: string = "";

    maxPerPage: number;

    @ViewChild(HamburgerbuttonComponent) hamburgerButton: HamburgerbuttonComponent;

    constructor(
        private globals: Globals,
        private apiClient: ApiclientService
    ) { }

    ngOnInit(): void {
        // Starts on first page
        this.ifNext();

        // Gets max of cards per average when initialized
        this.getMaxPerPage();
    }

    ifNext() {
        // REQUEST: Define the request
        this.request = this.apiClient.read(this.nextPage);

        // RESPONSE
        this.response = this.request.subscribe((result) => {

            // Get next page if exists
            this.nextPage = result.next;

            // Update previous page
            this.previousPage = result.previous;


            let url;
            if(this.nextPage != null) {
                url = new URL(this.nextPage);

                this.currentPage = this.globals.shortcuts.get("people") + "/?page=" + (Number(url.searchParams.get("page")) - 1);
            } else {
                // FOR THE LAST PAGE. SEE getCurrentPage() for more info.

                url = new URL(this.previousPage);
                this.currentPage = this.globals.shortcuts.get("people") + "/?page=" + (Number(url.searchParams.get("page")));
            }

            // Get final response
            this.response = result;
        });
    }

    ifPrevious() {
        // REQUEST: Define the request
        this.request = this.apiClient.read(this.previousPage);

        // RESPONSE
        this.response = this.request.subscribe((result) => {
            // Get previous page if exists
            this.previousPage = result.previous;

            // Update next page
            this.nextPage = result.next;

            let url;
            if(this.previousPage != null) {
                url = new URL(this.previousPage);

                this.currentPage = this.globals.shortcuts.get("people") + "/?page=" + (Number(url.searchParams.get("page")) + 1);
            } else {
                // FOR THE FIRST PAGE. SEE getCurrentPage() for more info.

                url = new URL(this.nextPage);
                this.currentPage = this.globals.shortcuts.get("people") + "/?page=" + (Number(url.searchParams.get("page")));
            }

            // Get final response
            this.response = result;
        });
    }

    // GET THE MAX AVERAGE OF CARDS PER PAGE. IT GETS THE ARRAY 'RESULTS' LENGTH OF THE FIRST PAGE 
    getMaxPerPage() {
        // REQUEST: Define the request
        this.request = this.apiClient.read(this.globals.shortcuts.get("people") + "/?page=1");

        // RESPONSE
        this.response = this.request.subscribe((result) => {
            // Get final response
            this.maxPerPage = result.results.length;
        });
    }


    // THIS IS THE BEST SOLUTION I FOUND FOR DISPLAYING THE CURRENT CHARACTERS' PAGE. I HAD TO CORRECT THE NUMBER DEPENDING ON THE API RESPONSE
    getCurrentPage() {
        let url = new URL(this.currentPage);
        let page: number;

        if(this.nextPage == null) {
            // IF WE REQUEST THE LAST PAGE OF CHARACTERS DIRECTORY
            page = Number(url.searchParams.get("page")) + 1;
        } else if (this.previousPage == null && (this.currentPage == this.nextPage)) {
            // IF WE REQUEST THE FIRST PAGE OF CHARACTERS DIRECTORY
            page = Number(url.searchParams.get("page")) - 1;
        } else {
            page = Number(url.searchParams.get("page"));
        }

        return page;
    }
}
