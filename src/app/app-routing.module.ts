import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterListComponent } from './views/character-list/character-list.component';
import { CharacterDetailsComponent } from './views/character-details/character-details.component';
import { HomeComponent } from './views/home/home.component';
import { NotFoundComponent } from './views/not-found/not-found.component';


const routes: Routes = [

    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'characters', component: CharacterListComponent },
    { path: 'characters/:id', component: CharacterDetailsComponent },
    { path: '404', component: NotFoundComponent },
    { path: '**', redirectTo: '/404' },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
