import { Component, OnInit, Renderer2, Input, ElementRef } from '@angular/core';

@Component({
    selector: 'charcard',
    templateUrl: './charcard.component.html',
    styleUrls: ['./charcard.component.css']
})
export class CharcardComponent implements OnInit {

    @Input() text: string;
    @Input() width: string;
    @Input() height: string;

    // Specific attributes
    @Input() date: string;
    @Input() gender: string;
    @Input() hairColor: string;
    @Input() url: string;
    genderImg: string; // A PATH

    // Get element reflection
    nativeElement = this.hostElement.nativeElement as HTMLElement;

    // Get element renderer for safe manipulation
    renderedElement = this.elementRenderer.selectRootElement(this.nativeElement);

    constructor(
        private hostElement: ElementRef, 
        private elementRenderer: Renderer2
    ) { }

    ngOnInit() {

        this.styling();

        if (this.gender == "male") {
            this.genderImg = "assets/icon/male.png";
        } else if (this.gender == "female") {
            this.genderImg = "assets/icon/female.png";
        } else {
            this.genderImg = "assets/icon/unknown2.png"
        }
    }

    styling() {
        this.renderedElement.style.width = this.width + "px";
        this.renderedElement.style.height = this.height + "px";
        this.renderedElement.style.position = "relative";
    }

    getCharacterId() {
        let slicedUrl = this.url.split("/");
        return slicedUrl[5];
    }

}
