import { Component, OnInit, Input, ElementRef, Renderer2, HostListener } from '@angular/core';

@Component({
    selector: 'hamburgerbutton',
    templateUrl: './hamburgerbutton.component.html',
    styleUrls: ['./hamburgerbutton.component.css']
})
export class HamburgerbuttonComponent implements OnInit {

    // Get element reflection
    nativeElement = this.hostElement.nativeElement as HTMLElement;

    // Get element renderer for safe manipulation
    renderedElement = this.elementRenderer.selectRootElement(this.nativeElement);

    @Input() text: string;
    @Input() width: string;
    @Input() height: string;

    expanded: boolean = false;
    visible: boolean = false;

    constructor(
        private hostElement: ElementRef, 
        private elementRenderer: Renderer2
    ) { }

    ngOnInit() {
        if(window.innerWidth <= 600) {
            this.visible = true;
        } else {
            this.visible = false;
        }

        this.styling();
    }

    // IT'S JUST AN ON/OFF SWITCH
    onClick() {
        this.expanded = !this.expanded;
    }

    styling() {
        this.renderedElement.style.width = this.width + "px";
        this.renderedElement.style.height = this.height + "px";
        this.renderedElement.style.position = "relative";
        this.renderedElement.style.display = "block";
        this.renderedElement.innerHTML = this.text;
        this.renderedElement.style.cursor = "pointer";
    }

    @HostListener("window:resize")
    onResize() {
        if(window.innerWidth <= 600) { // Under 600px
            this.visible = true;
        } else {
            this.visible = false;
        }
    }

}
