import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HamburgerbuttonComponent } from './hamburgerbutton.component';

describe('HamburguerbuttonComponent', () => {
  let component: HamburgerbuttonComponent;
  let fixture: ComponentFixture<HamburgerbuttonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HamburgerbuttonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HamburgerbuttonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
