import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'simplifyHair'
})
export class SimplifyHairPipe implements PipeTransform {
    transform(value: any): any {
        if(value == "none" || value == "n/a") {
            return "no hair";
        } else {
            return value;
        }
    }
}
